<?php

require "configb.php";

class MyUserClass
{
	
	public $dbconn;

	function __construct()
	{

    //Mudança para o PDO, mais seguro
	$this->conn = new PDO('mysql:host='.host.';dbname='.db.'', username, password);
    $this->dbconn->exec("set names utf8");      
  	}

	public function getUserList()
	{
		//retirei a comunicação com o bando desta classe.
		//$dbconn = new DatabaseConnection('localhost','user','password');
		//mudei query por prepare,
		$stmt =  $this->dbconn->prepare("SELECT name FROM user ORDER BY 1");
		$stmt->execute();
    	$results = $stmt->fetchAll();
	 	
	 	//A ordenação ficou para o banco com o order by 1,
	 	//pois nem toda aplicação tem uma máquina boa, geralmente investe mais em servidores de banco	 	

	 	return $results;
	}
}

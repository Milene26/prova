var app = angular.module('prova', []);

app.controller('tarefaController', function($scope, $http) {
	$scope.tarefas = [];
	$scope.botao = "Salvar";
	$scope.index = 0;

    $http.get(url_ws+"tarefas").success(function(data){
	    $scope.tarefas = data;
	}).error(function(error){
		$scope.tarefas = [];
	});

	$scope.salvar = function(){
		if($scope.botao == "Salvar"){
			if($scope.titulo == undefined || $scope.titulo == ""){
				swal ("Ops ..." , "Digite o título!" , "error");
			}else if($scope.descricao == undefined || $scope.descricao == ""){
				swal ("Ops ..." , "Digite a descrição!" , "error");				
			}else{
				tarefa = {titulo: $scope.titulo, descricao: $scope.descricao};

				$http.post(url_ws+"tarefas", tarefa).success(function(data){
				    if(data == '"cadastrado"'){
				    	$scope.tarefas.push(tarefa);

				    	$scope.titulo = "";
				    	$scope.descricao = "";
				    }
				}).error(function(error){
				    console.log(error);
				});
			}
		}else{
			tarefa = {idtarefa: $scope.idtarefa, titulo: $scope.titulo, descricao: $scope.descricao};

			$http.put(url_ws+"tarefas", tarefa).success(function(data){
			    if(data == '"alterado"'){
					$scope.tarefas[$scope.index].titulo = $scope.titulo;
					$scope.tarefas[$scope.index].descricao = $scope.descricao;

			    	$scope.titulo = "";
			    	$scope.descricao = "";
			    	$scope.idtarefa = "";			
			    	swal ("Cadastro Alterado!");    	
			    }
			}).error(function(error){
			    console.log(error);
			});
		}
	};

	$scope.remover = function(tarefa){
		var index = $scope.tarefas.indexOf(tarefa);

		$http.delete(url_ws+"tarefas/"+tarefa.idtarefa).success(function(data){
		    if(data === '"deletado"'){
		    	if(index > 0){
		    		console.log(index);
		    		$scope.tarefas.splice(index, 1);
		    		swal ("Cadastro Removido!");
		    	}
		    }else{
		    	swal ("Ops ..." , "Falha ao remover!" , "error");		    	
		    }
		}).error(function(error){
		    console.log(error);
		});
	};

	$scope.editar = function(tarefa){
		var index = $scope.tarefas.indexOf(tarefa);

		$scope.titulo = tarefa.titulo;
		$scope.descricao = tarefa.descricao;
		$scope.idtarefa = tarefa.idtarefa;
		$scope.botao = "Alterar";
		$scope.index = index;		
	};
});
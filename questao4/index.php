<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>
		Prova Milene
	</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 	<!-- importações css -->
  	<link rel="stylesheet" type="text/css" href="assets/css/estilo.css">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/sweetalert2.min.css">
</head>
<body>

	<div ng-app="prova" ng-controller="tarefaController">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">				
					<input type="hidden" ng-model="idtarefa" value="">
					<input type="hidden" ng-model="codigo" value="">
					
					<div class="col-sm-12">
						Título: <input type="text" ng-model="titulo" class="form-control"><br>
					</div>

					<div class="col-sm-12">
						Descrição: <input type="text" ng-model="descricao" class="form-control"><br>
					</div>
				<div class="col-sm-12">
					<button type="text" value="Salvar" class="btn btn-success" ng-click="salvar()"> 
              		 	{{botao}}
        			</button>
				</div>

				</div>

			</div>			
		</div>
		
		
		<br>

        <!-- lista das tarefas -->
        <div class="row">
        	<div class="col-sm-12">        		
		        <div class="list">
		        	<table class="table table-bordered">
		        		<thead>
		        			<tr>
		        				<th>
		        					Título
		        				</th>
		        				<th>
		        					Descrição
		        				</th>
		        				<th>
		        					Ações
		        				</th>
		        			</tr>
		        		</thead>

		        		<tbody>
		        			<tr ng-repeat="t in tarefas">
		        				<td>
		        					{{t.titulo}}
		        				</td>
		        				<td>
		        					{{t.descricao}}
		        				</td>
		        				<td>
		        					<a href="javascript:;" ng-click="editar(t)">Editar</a>
		        					<a href="" ng-click="remover(t)">Remover</a>
		        				</td>
		        			</tr>
		        		</tbody>
		        	</table>
		        </div>
        	</div>
        </div>
	</div>
	<!-- Scripts -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
	<script src="assets/js/config.js"></script>
	<script src="assets/js/sweetalert2.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.8/angular.min.js"></script>
	<script src="assets/js/tarefaController.js"></script>

	
</body>
</html>
<?php

// get all
$app->get('/tarefas', function(){
	$model = new \app\models\tarefa();
	$util = new \app\classes\util();

	$tarefas = $model->listar();

	$a = [];
	if($tarefas){
		foreach($tarefas as $key => $value){
			$a[] = $value->to_array();
			$a[$key]['datacadastro'] = $util->dataHifenBarraComT($a[$key]['datacadastro']);
		}
	}

	echo json_encode($a);
});

// update
$app->put("/tarefas", function() use ($app){
	$model = new \app\models\tarefa();
	$dados = json_decode($app->request()->getBody());

	$param = array(
		'titulo' => $dados->titulo,
		'descricao' => $dados->descricao
	);

	$retorno = $model->atualizar($dados->idtarefa, $param);

	if($retorno)
		$msg = "alterado";
	else
		$msg = "falha";

	echo json_encode($msg);
});

// insert
$app->post("/tarefas", function() use ($app){
	$model = new \app\models\tarefa();
	$dados = json_decode($app->request()->getBody());

	$param = array(
		'titulo' => $dados->titulo,
		'descricao' => $dados->descricao
	);

	$retorno = $model->cadastrar($param);

	if($retorno)
		$msg = "cadastrado";
	else
		$msg = "falha";

	echo json_encode($msg);
});

// delete
$app->delete('/tarefas/:idTarefa', function($idTarefa){
	$model = new \app\models\tarefa();

	try{
		$model->deletar($idTarefa);
		$msg = "deletado";
	}catch(Exception $e){
		$msg = "falha";
	}

	echo json_encode($msg);
});
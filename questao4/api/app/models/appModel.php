<?php

namespace app\models;

class appModel extends \ActiveRecord\Model{

	public function listar(){
		return parent::find('all');
	}

    public function pegar_pelo_id($campo, $valor, $tipo = null) {
        $tipoListagem = ($tipo == null) ? 'first' : 'all';
        return parent::find($tipoListagem, array('conditions' => array($campo . '=?', $valor)));
    }
	public function findTwoAtributtes($campo, $valor,$campo2, $valor2) {

        return parent::find('all', array($campo => $valor,$campo2 => $valor2));

        // return parent::find($tipoListagem, array('conditions' => array($campo . '=?', $valor,$campo2 . '=?', $valor2)));
    }

    public function atualizar($id, $attributes) {
        $atualizar = parent::find($id);

        $atualizar->update_attributes($attributes);
        return $atualizar;
    }

    public function atualizarPorFk($campo,$valor, $attributes) {
        $atualizar = parent::find('first', array('conditions' => array($campo . '=?', $valor)));
        $atualizar->update_attributes($attributes);
        return $atualizar;
    }

    public function deletar($id) {
        $deletar = parent::find($id);
        return $deletar->delete();
    }
    public function deletarFrom($campo,$valor) {
        $deletar = parent::find('all', array('conditions' => array($campo . '=?', $valor)));
        print_r($deletar);die;
        return $deletar->delete();
    }

    public function cadastrar($attributes) {
        $cadastrar = parent::create($attributes);
        return $cadastrar;
    }

}
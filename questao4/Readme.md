1)
Colocar Fonte index.php e pasta assets na raiz do projeto

2) 
Criar pasta api

3)
Copiar fontes da raiz da pasta api.
autoload.php, composer.json, config.php, connection.php, index.php

4)
No terminal, ir até a pasta 'api' e rodar os comandos composer install | composer update para instalar as dependências na pasta vendor.

5)
Ir até o arquivo connection.php, e alterar os dados conforme utilizados em desenvolvimento local ou remoto,
de acordo com modelo da linha 7

6)
Alterar no arquivo api/app/index.php, na linha 3, o caminho absoluto parar a pasta do framework.

7)
No arquivo assets/js/config.js, alterar a url utilizada no webservice para a correspondente.

8)
Para o banco importe o arquivo prova.php no seu http://localhost/phpmyadmin/
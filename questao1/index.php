<?php
/*
Escreva um programa que imprima números de 1 a 100. Mas, para múltiplos de 3 imprima
“Fizz” em vez do número e para múltiplos de 5 imprima “Buzz”. Para números múltiplos
de ambos (3 e 5), imprima “FizzBuzz”.
*/
class Multiplo
{
    public function multiplo($inicio, $final)
    {
    	$resultado = '';
		for ($numero=$inicio; $numero <= $final ; $numero++) { 

			//multiplo de 3
			if($numero % 3 == 0){
				$resultado .= "Fizz"."<br>";
			}elseif($numero % 5 == 0){//multiplo de 5
				$resultado .= "Buzz"."<br>";
			}elseif($numero % 3 == 0  && $numero % 5 == 0){//multiplo de 3 e 5															
				$resultado .= "FizzBuzz"."<br>";
			}else{
				$resultado .= $numero."<br>";
			}

		}
		return $resultado;
	}
}

$multiplo = new Multiplo;
echo $multiplo->multiplo(1, 100);

?>